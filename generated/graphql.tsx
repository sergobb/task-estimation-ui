import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type AddEstimationInput = {
  capacity: Scalars['Float'];
  complexity: Scalars['Float'];
  stage: Scalars['Float'];
  storypoints: Scalars['Float'];
  task: Scalars['Float'];
  uncertainty: Scalars['Float'];
  user: Scalars['Float'];
};

export type AddTaskInput = {
  name: Scalars['String'];
  team: Scalars['Float'];
};

export type AddTeamInput = {
  name: Scalars['String'];
};

export type AddUserInput = {
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  password: Scalars['String'];
};

export type CloseTaskInput = {
  id: Scalars['Float'];
  result: Scalars['Float'];
};

export type Estimation = {
  __typename?: 'Estimation';
  capacity: Scalars['Float'];
  complexity: Scalars['Float'];
  creationDate: Scalars['String'];
  id: Scalars['Float'];
  stage: Scalars['Float'];
  storypoints: Scalars['Float'];
  task?: Maybe<Task>;
  uncertainty: Scalars['Float'];
  user?: Maybe<User>;
};

export type GetTaskInput = {
  id?: Maybe<Scalars['Float']>;
  state?: Maybe<Scalars['Float']>;
  team?: Maybe<Scalars['Float']>;
};

export type GetTeamInput = {
  id: Scalars['Float'];
};

export type LoginUserInput = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  addEstimation: Estimation;
  addTask: Task;
  addTeam: Team;
  closeTask: Scalars['Boolean'];
  login: User;
  register: User;
  selectTeam: User;
};


export type MutationAddEstimationArgs = {
  data: AddEstimationInput;
};


export type MutationAddTaskArgs = {
  data: AddTaskInput;
};


export type MutationAddTeamArgs = {
  data: AddTeamInput;
};


export type MutationCloseTaskArgs = {
  data: CloseTaskInput;
};


export type MutationLoginArgs = {
  data: LoginUserInput;
};


export type MutationRegisterArgs = {
  data: AddUserInput;
};


export type MutationSelectTeamArgs = {
  data: SelectTeamInput;
};

export type Query = {
  __typename?: 'Query';
  getAllTasks: Array<Task>;
  getAllTeams: Array<Team>;
  getAllUsers: Array<User>;
  getTasks: Array<Task>;
  getTeamById: Team;
  getTeamByUser: Team;
};


export type QueryGetTasksArgs = {
  data: GetTaskInput;
};


export type QueryGetTeamByIdArgs = {
  data: GetTeamInput;
};


export type QueryGetTeamByUserArgs = {
  data: GetTeamInput;
};

export type SelectTeamInput = {
  teamId: Scalars['Float'];
  userId: Scalars['Float'];
};

export type Task = {
  __typename?: 'Task';
  estimation?: Maybe<Scalars['Float']>;
  id: Scalars['Float'];
  name: Scalars['String'];
  result?: Maybe<Scalars['Float']>;
  state?: Maybe<Scalars['Float']>;
  team?: Maybe<Team>;
};

export type Team = {
  __typename?: 'Team';
  id: Scalars['Float'];
  name: Scalars['String'];
  tasks?: Maybe<Array<Task>>;
  users?: Maybe<Array<User>>;
};

export type User = {
  __typename?: 'User';
  email: Scalars['String'];
  firstName: Scalars['String'];
  id: Scalars['Float'];
  lastName: Scalars['String'];
  team?: Maybe<Team>;
};

export type AddEstimationMutationVariables = Exact<{
  addEstimationData: AddEstimationInput;
}>;


export type AddEstimationMutation = { __typename?: 'Mutation', addEstimation: { __typename?: 'Estimation', id: number } };

export type AddTaskMutationVariables = Exact<{
  addTaskData: AddTaskInput;
}>;


export type AddTaskMutation = { __typename?: 'Mutation', addTask: { __typename?: 'Task', id: number, name: string } };

export type CloseTaskMutationVariables = Exact<{
  closeTaskData: CloseTaskInput;
}>;


export type CloseTaskMutation = { __typename?: 'Mutation', closeTask: boolean };

export type CreateTeamMutationVariables = Exact<{
  addTeamData: AddTeamInput;
}>;


export type CreateTeamMutation = { __typename?: 'Mutation', addTeam: { __typename?: 'Team', id: number, name: string } };

export type GetAllTeamsQueryQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllTeamsQueryQuery = { __typename?: 'Query', getAllTeams: Array<{ __typename?: 'Team', id: number, name: string }> };

export type GetTasksQueryVariables = Exact<{
  getTasksData: GetTaskInput;
}>;


export type GetTasksQuery = { __typename?: 'Query', getTasks: Array<{ __typename?: 'Task', id: number, name: string, state?: number | null | undefined, estimation?: number | null | undefined, result?: number | null | undefined }> };

export type GetTeamByIdQueryVariables = Exact<{
  getTeamByIdData: GetTeamInput;
}>;


export type GetTeamByIdQuery = { __typename?: 'Query', getTeamById: { __typename?: 'Team', id: number, name: string, users?: Array<{ __typename?: 'User', id: number, firstName: string, lastName: string, email: string }> | null | undefined, tasks?: Array<{ __typename?: 'Task', id: number, name: string, state?: number | null | undefined, estimation?: number | null | undefined, result?: number | null | undefined }> | null | undefined } };

export type GetTeamByUserQueryVariables = Exact<{
  getTeamByUserData: GetTeamInput;
}>;


export type GetTeamByUserQuery = { __typename?: 'Query', getTeamByUser: { __typename?: 'Team', id: number, name: string, users?: Array<{ __typename?: 'User', id: number, firstName: string, lastName: string, email: string }> | null | undefined, tasks?: Array<{ __typename?: 'Task', id: number, name: string, state?: number | null | undefined, estimation?: number | null | undefined, result?: number | null | undefined }> | null | undefined } };

export type LoginUserMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginUserMutation = { __typename?: 'Mutation', login: { __typename?: 'User', id: number, firstName: string, lastName: string, email: string } };

export type RegisterUserMutationVariables = Exact<{
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type RegisterUserMutation = { __typename?: 'Mutation', register: { __typename?: 'User', id: number, firstName: string, lastName: string, email: string } };

export type SelectTeamMutationVariables = Exact<{
  selectTeamData: SelectTeamInput;
}>;


export type SelectTeamMutation = { __typename?: 'Mutation', selectTeam: { __typename?: 'User', id: number, firstName: string, lastName: string, team?: { __typename?: 'Team', id: number, name: string } | null | undefined } };


export const AddEstimationDocument = gql`
    mutation AddEstimation($addEstimationData: AddEstimationInput!) {
  addEstimation(data: $addEstimationData) {
    id
  }
}
    `;
export type AddEstimationMutationFn = Apollo.MutationFunction<AddEstimationMutation, AddEstimationMutationVariables>;

/**
 * __useAddEstimationMutation__
 *
 * To run a mutation, you first call `useAddEstimationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddEstimationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addEstimationMutation, { data, loading, error }] = useAddEstimationMutation({
 *   variables: {
 *      addEstimationData: // value for 'addEstimationData'
 *   },
 * });
 */
export function useAddEstimationMutation(baseOptions?: Apollo.MutationHookOptions<AddEstimationMutation, AddEstimationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddEstimationMutation, AddEstimationMutationVariables>(AddEstimationDocument, options);
      }
export type AddEstimationMutationHookResult = ReturnType<typeof useAddEstimationMutation>;
export type AddEstimationMutationResult = Apollo.MutationResult<AddEstimationMutation>;
export type AddEstimationMutationOptions = Apollo.BaseMutationOptions<AddEstimationMutation, AddEstimationMutationVariables>;
export const AddTaskDocument = gql`
    mutation AddTask($addTaskData: AddTaskInput!) {
  addTask(data: $addTaskData) {
    id
    name
  }
}
    `;
export type AddTaskMutationFn = Apollo.MutationFunction<AddTaskMutation, AddTaskMutationVariables>;

/**
 * __useAddTaskMutation__
 *
 * To run a mutation, you first call `useAddTaskMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddTaskMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addTaskMutation, { data, loading, error }] = useAddTaskMutation({
 *   variables: {
 *      addTaskData: // value for 'addTaskData'
 *   },
 * });
 */
export function useAddTaskMutation(baseOptions?: Apollo.MutationHookOptions<AddTaskMutation, AddTaskMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddTaskMutation, AddTaskMutationVariables>(AddTaskDocument, options);
      }
export type AddTaskMutationHookResult = ReturnType<typeof useAddTaskMutation>;
export type AddTaskMutationResult = Apollo.MutationResult<AddTaskMutation>;
export type AddTaskMutationOptions = Apollo.BaseMutationOptions<AddTaskMutation, AddTaskMutationVariables>;
export const CloseTaskDocument = gql`
    mutation CloseTask($closeTaskData: CloseTaskInput!) {
  closeTask(data: $closeTaskData)
}
    `;
export type CloseTaskMutationFn = Apollo.MutationFunction<CloseTaskMutation, CloseTaskMutationVariables>;

/**
 * __useCloseTaskMutation__
 *
 * To run a mutation, you first call `useCloseTaskMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCloseTaskMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [closeTaskMutation, { data, loading, error }] = useCloseTaskMutation({
 *   variables: {
 *      closeTaskData: // value for 'closeTaskData'
 *   },
 * });
 */
export function useCloseTaskMutation(baseOptions?: Apollo.MutationHookOptions<CloseTaskMutation, CloseTaskMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CloseTaskMutation, CloseTaskMutationVariables>(CloseTaskDocument, options);
      }
export type CloseTaskMutationHookResult = ReturnType<typeof useCloseTaskMutation>;
export type CloseTaskMutationResult = Apollo.MutationResult<CloseTaskMutation>;
export type CloseTaskMutationOptions = Apollo.BaseMutationOptions<CloseTaskMutation, CloseTaskMutationVariables>;
export const CreateTeamDocument = gql`
    mutation createTeam($addTeamData: AddTeamInput!) {
  addTeam(data: $addTeamData) {
    id
    name
  }
}
    `;
export type CreateTeamMutationFn = Apollo.MutationFunction<CreateTeamMutation, CreateTeamMutationVariables>;

/**
 * __useCreateTeamMutation__
 *
 * To run a mutation, you first call `useCreateTeamMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTeamMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTeamMutation, { data, loading, error }] = useCreateTeamMutation({
 *   variables: {
 *      addTeamData: // value for 'addTeamData'
 *   },
 * });
 */
export function useCreateTeamMutation(baseOptions?: Apollo.MutationHookOptions<CreateTeamMutation, CreateTeamMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateTeamMutation, CreateTeamMutationVariables>(CreateTeamDocument, options);
      }
export type CreateTeamMutationHookResult = ReturnType<typeof useCreateTeamMutation>;
export type CreateTeamMutationResult = Apollo.MutationResult<CreateTeamMutation>;
export type CreateTeamMutationOptions = Apollo.BaseMutationOptions<CreateTeamMutation, CreateTeamMutationVariables>;
export const GetAllTeamsQueryDocument = gql`
    query getAllTeamsQuery {
  getAllTeams {
    id
    name
  }
}
    `;

/**
 * __useGetAllTeamsQueryQuery__
 *
 * To run a query within a React component, call `useGetAllTeamsQueryQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllTeamsQueryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllTeamsQueryQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllTeamsQueryQuery(baseOptions?: Apollo.QueryHookOptions<GetAllTeamsQueryQuery, GetAllTeamsQueryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAllTeamsQueryQuery, GetAllTeamsQueryQueryVariables>(GetAllTeamsQueryDocument, options);
      }
export function useGetAllTeamsQueryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAllTeamsQueryQuery, GetAllTeamsQueryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAllTeamsQueryQuery, GetAllTeamsQueryQueryVariables>(GetAllTeamsQueryDocument, options);
        }
export type GetAllTeamsQueryQueryHookResult = ReturnType<typeof useGetAllTeamsQueryQuery>;
export type GetAllTeamsQueryLazyQueryHookResult = ReturnType<typeof useGetAllTeamsQueryLazyQuery>;
export type GetAllTeamsQueryQueryResult = Apollo.QueryResult<GetAllTeamsQueryQuery, GetAllTeamsQueryQueryVariables>;
export const GetTasksDocument = gql`
    query getTasks($getTasksData: GetTaskInput!) {
  getTasks(data: $getTasksData) {
    id
    name
    state
    estimation
    result
  }
}
    `;

/**
 * __useGetTasksQuery__
 *
 * To run a query within a React component, call `useGetTasksQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTasksQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTasksQuery({
 *   variables: {
 *      getTasksData: // value for 'getTasksData'
 *   },
 * });
 */
export function useGetTasksQuery(baseOptions: Apollo.QueryHookOptions<GetTasksQuery, GetTasksQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTasksQuery, GetTasksQueryVariables>(GetTasksDocument, options);
      }
export function useGetTasksLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTasksQuery, GetTasksQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTasksQuery, GetTasksQueryVariables>(GetTasksDocument, options);
        }
export type GetTasksQueryHookResult = ReturnType<typeof useGetTasksQuery>;
export type GetTasksLazyQueryHookResult = ReturnType<typeof useGetTasksLazyQuery>;
export type GetTasksQueryResult = Apollo.QueryResult<GetTasksQuery, GetTasksQueryVariables>;
export const GetTeamByIdDocument = gql`
    query getTeamById($getTeamByIdData: GetTeamInput!) {
  getTeamById(data: $getTeamByIdData) {
    id
    name
    users {
      id
      firstName
      lastName
      email
    }
    tasks {
      id
      name
      state
      estimation
      result
    }
  }
}
    `;

/**
 * __useGetTeamByIdQuery__
 *
 * To run a query within a React component, call `useGetTeamByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamByIdQuery({
 *   variables: {
 *      getTeamByIdData: // value for 'getTeamByIdData'
 *   },
 * });
 */
export function useGetTeamByIdQuery(baseOptions: Apollo.QueryHookOptions<GetTeamByIdQuery, GetTeamByIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamByIdQuery, GetTeamByIdQueryVariables>(GetTeamByIdDocument, options);
      }
export function useGetTeamByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamByIdQuery, GetTeamByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamByIdQuery, GetTeamByIdQueryVariables>(GetTeamByIdDocument, options);
        }
export type GetTeamByIdQueryHookResult = ReturnType<typeof useGetTeamByIdQuery>;
export type GetTeamByIdLazyQueryHookResult = ReturnType<typeof useGetTeamByIdLazyQuery>;
export type GetTeamByIdQueryResult = Apollo.QueryResult<GetTeamByIdQuery, GetTeamByIdQueryVariables>;
export const GetTeamByUserDocument = gql`
    query getTeamByUser($getTeamByUserData: GetTeamInput!) {
  getTeamByUser(data: $getTeamByUserData) {
    id
    name
    users {
      id
      firstName
      lastName
      email
    }
    tasks {
      id
      name
      state
      estimation
      result
    }
  }
}
    `;

/**
 * __useGetTeamByUserQuery__
 *
 * To run a query within a React component, call `useGetTeamByUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamByUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamByUserQuery({
 *   variables: {
 *      getTeamByUserData: // value for 'getTeamByUserData'
 *   },
 * });
 */
export function useGetTeamByUserQuery(baseOptions: Apollo.QueryHookOptions<GetTeamByUserQuery, GetTeamByUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTeamByUserQuery, GetTeamByUserQueryVariables>(GetTeamByUserDocument, options);
      }
export function useGetTeamByUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTeamByUserQuery, GetTeamByUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTeamByUserQuery, GetTeamByUserQueryVariables>(GetTeamByUserDocument, options);
        }
export type GetTeamByUserQueryHookResult = ReturnType<typeof useGetTeamByUserQuery>;
export type GetTeamByUserLazyQueryHookResult = ReturnType<typeof useGetTeamByUserLazyQuery>;
export type GetTeamByUserQueryResult = Apollo.QueryResult<GetTeamByUserQuery, GetTeamByUserQueryVariables>;
export const LoginUserDocument = gql`
    mutation loginUser($email: String!, $password: String!) {
  login(data: {email: $email, password: $password}) {
    id
    firstName
    lastName
    email
  }
}
    `;
export type LoginUserMutationFn = Apollo.MutationFunction<LoginUserMutation, LoginUserMutationVariables>;

/**
 * __useLoginUserMutation__
 *
 * To run a mutation, you first call `useLoginUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginUserMutation, { data, loading, error }] = useLoginUserMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginUserMutation(baseOptions?: Apollo.MutationHookOptions<LoginUserMutation, LoginUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginUserMutation, LoginUserMutationVariables>(LoginUserDocument, options);
      }
export type LoginUserMutationHookResult = ReturnType<typeof useLoginUserMutation>;
export type LoginUserMutationResult = Apollo.MutationResult<LoginUserMutation>;
export type LoginUserMutationOptions = Apollo.BaseMutationOptions<LoginUserMutation, LoginUserMutationVariables>;
export const RegisterUserDocument = gql`
    mutation registerUser($firstName: String!, $lastName: String!, $email: String!, $password: String!) {
  register(
    data: {firstName: $firstName, lastName: $lastName, email: $email, password: $password}
  ) {
    id
    firstName
    lastName
    email
  }
}
    `;
export type RegisterUserMutationFn = Apollo.MutationFunction<RegisterUserMutation, RegisterUserMutationVariables>;

/**
 * __useRegisterUserMutation__
 *
 * To run a mutation, you first call `useRegisterUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerUserMutation, { data, loading, error }] = useRegisterUserMutation({
 *   variables: {
 *      firstName: // value for 'firstName'
 *      lastName: // value for 'lastName'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useRegisterUserMutation(baseOptions?: Apollo.MutationHookOptions<RegisterUserMutation, RegisterUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterUserMutation, RegisterUserMutationVariables>(RegisterUserDocument, options);
      }
export type RegisterUserMutationHookResult = ReturnType<typeof useRegisterUserMutation>;
export type RegisterUserMutationResult = Apollo.MutationResult<RegisterUserMutation>;
export type RegisterUserMutationOptions = Apollo.BaseMutationOptions<RegisterUserMutation, RegisterUserMutationVariables>;
export const SelectTeamDocument = gql`
    mutation SelectTeam($selectTeamData: SelectTeamInput!) {
  selectTeam(data: $selectTeamData) {
    id
    firstName
    lastName
    team {
      id
      name
    }
  }
}
    `;
export type SelectTeamMutationFn = Apollo.MutationFunction<SelectTeamMutation, SelectTeamMutationVariables>;

/**
 * __useSelectTeamMutation__
 *
 * To run a mutation, you first call `useSelectTeamMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSelectTeamMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [selectTeamMutation, { data, loading, error }] = useSelectTeamMutation({
 *   variables: {
 *      selectTeamData: // value for 'selectTeamData'
 *   },
 * });
 */
export function useSelectTeamMutation(baseOptions?: Apollo.MutationHookOptions<SelectTeamMutation, SelectTeamMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SelectTeamMutation, SelectTeamMutationVariables>(SelectTeamDocument, options);
      }
export type SelectTeamMutationHookResult = ReturnType<typeof useSelectTeamMutation>;
export type SelectTeamMutationResult = Apollo.MutationResult<SelectTeamMutation>;
export type SelectTeamMutationOptions = Apollo.BaseMutationOptions<SelectTeamMutation, SelectTeamMutationVariables>;