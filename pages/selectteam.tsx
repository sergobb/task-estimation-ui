import { Space, Form, Input, Button, Card } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import SkeletonCard from "../components/SkeletonCard";
import React, { useContext } from "react";
import { useGetAllTeamsQueryQuery, useLoginUserMutation, useSelectTeamMutation } from "../generated/graphql";
import classes from "../styles/index.module.css";
import { Select } from "antd";
import { Context } from "../context/context";

const { Option } = Select;

const layout = {
  // labelCol: { span: 3 },
  wrapperCol: { span: 21 },
};

const tailLayout = {
  wrapperCol: { span: 24 },
};
export default function selectTeam() {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id, setId } = useContext(Context);
  const { data, loading, error } = useGetAllTeamsQueryQuery({
    fetchPolicy: "network-only",
  });
  const [selectTeamMutation] = useSelectTeamMutation({
    variables: {
      selectTeamData: {
        userId: 0,
        teamId: 0,
      },
    },
  });

  const onFinish = async (values: any) => {
    await selectTeamMutation({
      variables: {
        selectTeamData: {
          userId: id,
          teamId: values.teams,
        },
      },
    });
    router.push("/");
  };

  const onCancel = () => {
    router.push("createteam");
  };

  if (loading || id==0) return <SkeletonCard title='Loading....' />;

  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Присоединяйтесь к команде</Title>
          <Form {...layout} onFinish={onFinish} initialValues={{ teams: data.getAllTeams[0].id }}>
            <Form.Item name='teams' label='Команды'>
              <Select id='teams'>
                {data.getAllTeams.map((item) => (
                  <Option key={item.id} value={item.id}>
                    {item.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Space align='end'>
                <Button type='primary' htmlType='submit'>
                  Присоединиться
                </Button>
                <Button htmlType='button' onClick={onCancel}>
                  Создать свою
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Card>
      </Space>
    </div>
  );
}
