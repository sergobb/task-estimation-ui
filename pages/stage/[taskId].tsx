import { Space, Card, Image, Descriptions, Button } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import React, { useContext, useState } from "react";
import classes from "../../styles/index.module.css";

export default function stage() {
  const router = useRouter();
  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Командная оценка</Title>
          <Descriptions size='small' column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
            <Descriptions.Item label='Этап оценивания'>1</Descriptions.Item>
            <Descriptions.Item label='Завершено'>{new Date().toDateString()}</Descriptions.Item>
          </Descriptions>
          <Image src='/points.png' />
          <Space align='end'>
            <Button type='primary' htmlType='submit' onClick={() => router.push("/")}>
              Завершить оценку
            </Button>
            <Button htmlType='button'>
              Повторить
            </Button>
          </Space>
        </Card>
      </Space>
    </div>
  );
}
