import { Space, Form, Input, Button, Card } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import React, { useContext } from "react";
import { useAddTaskMutation } from "../../generated/graphql";
import classes from "../../styles/index.module.css";
import { Select } from "antd";
import { Context } from "../../context/context";

const { Option } = Select;

const layout = {
  // labelCol: { span: 3 },
  wrapperCol: { span: 21 },
};

const tailLayout = {
  wrapperCol: { span: 24 },
};
export default function createTask() {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id, setId } = useContext(Context);
  const [ addTaskMutation ] = useAddTaskMutation({
    variables: {
      addTaskData: {
        name: "",
        team: 0,
      },
    },
  });

  const onFinish = async (values: any) => {
    await addTaskMutation({
      variables: {
        addTaskData: {
          name: values.name,
          team: +router.query.teamId
        },
      },
    });
    router.push("/");
  };

  const onCancel = () => {
    router.push("/");
  };

  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Добавить задачу в беклог</Title>
          <Form {...layout} onFinish={onFinish}>
            <Form.Item name='name' label='Задача' rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Space align='end'>
                <Button type='primary' htmlType='submit'>
                  Добавить
                </Button>
                <Button htmlType='button' onClick={onCancel}>
                  Вернуться
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Card>
      </Space>
    </div>
  );
}
