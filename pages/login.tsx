import { Space, Form, Input, Button, Card } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import React, { useContext } from "react";
import { Context } from "../context/context";
import { useLoginUserMutation } from "../generated/graphql";
import classes from "../styles/index.module.css";
import localForage from 'localforage';

const layout = {
  labelCol: { span: 3 },
  wrapperCol: { span: 21 },
};

const tailLayout = {
  wrapperCol: { span: 24 },
};
export default function login() {
  const [form] = Form.useForm();
  const router = useRouter();
  const {id, setId} = useContext(Context);
  const [loginUserMutation, { data, loading, error }] = useLoginUserMutation({
    variables: {
      email: "",
      password: ""
    },
  });

  const onFinish = async (values: any) => {
    const user = await loginUserMutation({ variables: values });
    setId(user.data.login.id)
    localForage.setItem("userId", user.data.login.id)
    router.push("selectteam");
  };

  const onCancel = () => {
    router.push("/");
  };

  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Вход с систему</Title>
          <Form {...layout} onFinish={onFinish}>
            <Form.Item name='email' label='E-mail' rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item name='password' label='Пароль' rules={[{ required: true }]}>
              <Input.Password placeholder='input password' />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Space align='end'>
                <Button type='primary' htmlType='submit'>
                  Войти
                </Button>
                <Button htmlType='button' onClick={onCancel}>Покинуть сайт</Button>
              </Space>
            </Form.Item>
          </Form>
        </Card>
      </Space>
    </div>
  );
}
