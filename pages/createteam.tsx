import { Space, Form, Input, Button, Card } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import SkeletonCard from "../components/SkeletonCard";
import React, { useContext } from "react";
import { useCreateTeamMutation, useGetAllTeamsQueryQuery, useLoginUserMutation, useSelectTeamMutation } from "../generated/graphql";
import classes from "../styles/index.module.css";
import { Select } from "antd";
import { Context } from "../context/context";

const { Option } = Select;

const layout = {
  // labelCol: { span: 3 },
  wrapperCol: { span: 21 },
};

const tailLayout = {
  wrapperCol: { span: 24 },
};
export default function createTeam() {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id, setId } = useContext(Context);
  // const { data, loading, error } = useGetAllTeamsQueryQuery({
  //   fetchPolicy: "network-only",
  // });
  const [createTeamMutation] = useCreateTeamMutation({
    variables: {
      addTeamData: {
          name: ""
      }
    },
  });
  const [selectTeamMutation] = useSelectTeamMutation({
    variables: {
      selectTeamData: {
        userId: 0,
        teamId: 0,
      },
    },
  });

  const onFinish = async (values: any) => {
    const team = await createTeamMutation({
      variables: {
        addTeamData: {
          name: values.team
        },
      },
    });
    await selectTeamMutation({
        variables: {
          selectTeamData: {
            userId: id,
            teamId: team.data.addTeam.id
          },
        },
      });
    router.push("/");
  };

  const onCancel = () => {
    router.push("selectteam");
  };

  // if (loading) return <SkeletonCard title='Loading....' />;

  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Создайте свою комнаду</Title>
          <Form {...layout} onFinish={onFinish} >
            <Form.Item name='team' label='Имя комнды' rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Space align='end'>
                <Button type='primary' htmlType='submit'>
                  Создать
                </Button>
                <Button htmlType='button' onClick={onCancel}>
                  Вернуться к выбору
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Card>
      </Space>
    </div>
  );
}
