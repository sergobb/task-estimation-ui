import { Space, Card, Image, Descriptions, Button } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import React, { useContext, useState } from "react";
import classes from "../../styles/index.module.css";

export default function stat() {
  const router = useRouter();
  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Статистика по задаче</Title>
          <Descriptions size='small' column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
            <Descriptions.Item label='Количество этапов оценивания'>3</Descriptions.Item>
            <Descriptions.Item label='Оценка'>2</Descriptions.Item>
            <Descriptions.Item label='По факту выполнения'>3</Descriptions.Item>
          </Descriptions>
          <Image src='/stats.png' />
          <Space align='end'>
            <Button type='primary' htmlType='submit' onClick={() => router.push("/")}>
              Закрыть
            </Button>
          </Space>
        </Card>
      </Space>
    </div>
  );
}
