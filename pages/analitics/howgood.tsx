import { Space, Card, Image } from "antd";
import Title from "antd/lib/typography/Title";
import React, { useContext, useState } from "react";
import classes from "../../styles/index.module.css";

export default function howgood() {
  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Насколько хорошо оцениваются задачи</Title>
          <Image src='/howgood.png' />
        </Card>
      </Space>
    </div>
  );
}
