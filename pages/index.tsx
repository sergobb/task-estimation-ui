import classes from "../styles/index.module.css";
import { Card, Space, Form, Input, Button, Descriptions, PageHeader, Divider, Tooltip, List, Avatar, Badge } from "antd";
import router from "next/dist/client/router";
import React, { useContext } from "react";
import AddButton from "../components/AddButton";
import { useGetTeamByUserQuery } from "../generated/graphql";
import SkeletonCard from "../components/SkeletonCard";
import { Context } from "../context/context";
import Title from "antd/lib/typography/Title";
import {
  CheckCircleTwoTone,
  ExclamationOutlined,
  FundTwoTone,
  HourglassTwoTone,
  NotificationOutlined,
  SettingOutlined,
  SlidersTwoTone,
} from "@ant-design/icons";
import Link from "antd/lib/typography/Link";

export default function Home() {
  const { id, setId } = useContext(Context);
  const { data, loading, error } = useGetTeamByUserQuery({
    variables: {
      getTeamByUserData: {
        id,
      },
    },
    fetchPolicy: "network-only",
  });

  if (loading) return <SkeletonCard title='Loading....' />;
  if (!id)
    return (
      <Space direction='vertical' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Link href='login'>Войдите </Link>
          или
          <Link href='register'> Зарегистрируйтесь</Link>
        </Card>
      </Space>
    );
  const team = data.getTeamByUser;
  const tasks = team.tasks;

  return (
    <div className={classes.Main}>
      <PageHeader
        ghost={false}
        title={team.name}
        extra={[
          <Tooltip key='3' title='Распределение задача по уровням сложности'>
            <Link href='analitics/estimated'>
              <Button icon={<FundTwoTone />}></Button>
            </Link>
          </Tooltip>,
          <Tooltip key='2' title='Насколько хорошо оцениваются задачи'>
            <Link href='analitics/howgood'>
              <Button icon={<SlidersTwoTone />}></Button>
            </Link>
          </Tooltip>,
        ]}
      ></PageHeader>
      <Space direction='vertical' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Новые задачи</Title>
          <List
            itemLayout='horizontal'
            dataSource={tasks.filter((task) => task.state === 0)}
            renderItem={(item) => (
              <List.Item key={item.id}>
                <List.Item.Meta
                  avatar={<HourglassTwoTone twoToneColor='#eb2f96' />}
                  title={<Link href={`estimate/${item.id}/0`}>{item.name}</Link>}
                />
              </List.Item>
            )}
          />
        </Card>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Оцененные задачи</Title>
          <List
            itemLayout='horizontal'
            dataSource={tasks.filter((task) => task.state === 1)}
            renderItem={(item) => (
              <List.Item key={item.id}>
                <List.Item.Meta avatar={<HourglassTwoTone />} title={<Link href={`closetask/${item.id}`}>{item.name}</Link>} />
                <Badge count={item.estimation} style={{ backgroundColor: "#108ee9" }} />
              </List.Item>
            )}
          />
        </Card>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Выполненные задачи</Title>
          <List
            itemLayout='horizontal'
            dataSource={tasks.filter((task) => task.state === 2)}
            renderItem={(item) => (
              <List.Item key={item.id}>
                <List.Item.Meta
                  avatar={<CheckCircleTwoTone twoToneColor='#52c41a' />}
                  title={<Link href={`taskstat/${item.id}`}>{item.name}</Link>}
                />
                <Badge count={item.estimation} style={{ backgroundColor: "#108ee9" }} />
                <Badge count={item.result} style={{ backgroundColor: "#52c41a" }} />
              </List.Item>
            )}
          />
        </Card>
      </Space>

      <AddButton toolTip='Внести задачу в беклог' onClick={() => router.push(`addtask/${team.id}`)} />
    </div>
  );
}
