import "antd/dist/antd.css";
import "../styles/vars.css";
import "../styles/global.css";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import { Context } from "../context/context";
import React, { useState } from "react";
import localForage from "localforage";
import SkeletonCard from "../components/SkeletonCard";

const serverUrl = "http://" + (process.env.SERVER!=undefined?process.env.SERVER:"localhost") + ":4000/graphql";
const client = new ApolloClient({
  uri: serverUrl,
  cache: new InMemoryCache(),
});

export default function MyApp({ Component, pageProps }) {
  localForage.config({
    driver: localForage.INDEXEDDB, // Force WebSQL; same as using setDriver()
    name: "task_estimation",
    version: 2.2,
    storeName: "session", // Should be alphanumeric, with underscores.
    description: "session data",
  });

  const [context, setContext] = useState(0);
  localForage.getItem<number>("userId").then(userId => {
    setContext(userId)
  });

  if (context == 0) return <SkeletonCard title='Loading....' />;
  return (
    <ApolloProvider client={client}>
      <Context.Provider value={{ id: context, setId: setContext }}>
        <Component {...pageProps} />
      </Context.Provider>
    </ApolloProvider>
  );
}
