import { Space, Form, Input, Button, Card, Descriptions, Col, InputNumber, Row, Slider } from "antd";
import Title from "antd/lib/typography/Title";
import { useRouter } from "next/dist/client/router";
import React, { useContext, useState } from "react";
import { useAddEstimationMutation, useAddTaskMutation, useGetTasksQuery } from "../../../generated/graphql";
import classes from "../../../styles/index.module.css";
import { Select } from "antd";
import { Context } from "../../../context/context";
import SkeletonCard from "../../../components/SkeletonCard";
import { FrownOutlined, FrownTwoTone, SmileOutlined, SmileTwoTone } from "@ant-design/icons";

const { Option } = Select;

const layout = {
  // labelCol: { span: 3 },
  wrapperCol: { span: 21 },
};

const tailLayout = {
  wrapperCol: { span: 24 },
};
export default function estimateTask() {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id, setId } = useContext(Context);
  const { data, loading, error } = useGetTasksQuery({
    variables: {
      getTasksData: {
        id: +router.query.taskId,
      },
    },
  });
  const [addEstimationMutation] = useAddEstimationMutation({
    variables: {
      addEstimationData: {
        stage: 0,
        user: 0,
        task: 0,
        complexity: 0,
        capacity: 0,
        uncertainty: 0,
        storypoints: 0
      },
    },
  });
  const [complexity, setComplexity] = useState(1);
  const [capacity, setCapacity] = useState(1);
  const [uncertainty, setUncertainty] = useState(1);
  const [result, setResult] = useState(1);

  const estimate = (complexity, capacity, uncertainty) => {
    const sum = complexity + capacity + uncertainty;
    switch (sum) {
      case 9:
        return 21;
      case 8:
        return 13;
      case 7:
        return 8;
      case 6:
        return 5;
      case 5:
        return 3;
      case 4:
        return 2;
      default:
        return 1;
    }
    return sum;
  };

  const onChangeComplexity = (value: number) => {
    setComplexity(() => value);
    setResult(() => estimate(value, capacity, uncertainty));
  };
  const onChangeCapacity = (value: number) => {
    setCapacity(() => value);
    setResult(() => estimate(complexity, value, uncertainty));
  };
  const onChangeUncertainty = (value: number) => {
    setUncertainty(() => value);
    setResult(() => estimate(complexity, capacity, value));
  };

  const onChangeResult = (value: string) => {
    setResult(+value);
  };

  const onFinish = async (values: any) => {
    await addEstimationMutation({
      variables: {
        addEstimationData: {
          user: id,
          task: +router.query.taskId,
          complexity,
          capacity,
          uncertainty,
          storypoints: result,
          stage: +router.query.stage
        },
      },
    });
    router.push(`/stage/${router.query.taskId}`);
  };

  const onCancel = () => {
    router.push("/");
  };

  if (loading || id == 0) return <SkeletonCard title='Loading....' />;

  const task = data.getTasks[0];
  console.log(router.query.stage);

  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={3}>Оцените задачу</Title>
          <Title level={5}>{task.name}</Title>
          <Row>
            <Col span={5}>Сложность</Col>
            <Col span={1}>
              <SmileTwoTone style={{ fontSize: 20 }} twoToneColor='#52c41a' />
            </Col>
            <Col span={17}>
              <Slider min={1} max={3} onChange={onChangeComplexity} value={complexity} />
            </Col>
            <Col span={1}>
              <FrownTwoTone style={{ fontSize: 20 }} twoToneColor='#eb2f96' />
            </Col>
          </Row>
          <Row>
            <Col span={5}>Трудоёмкость</Col>
            <Col span={1}>
              <SmileTwoTone style={{ fontSize: 20 }} twoToneColor='#52c41a' />
            </Col>
            <Col span={17}>
              <Slider min={1} max={3} onChange={onChangeCapacity} value={capacity} />
            </Col>
            <Col span={1}>
              <FrownTwoTone style={{ fontSize: 20 }} twoToneColor='#eb2f96' />
            </Col>
          </Row>
          <Row>
            <Col span={5}>Неопределенность</Col>
            <Col span={1}>
              <SmileTwoTone style={{ fontSize: 20 }} twoToneColor='#52c41a' />
            </Col>
            <Col span={17}>
              <Slider min={1} max={3} onChange={onChangeUncertainty} value={uncertainty} />
            </Col>
            <Col span={1}>
              <FrownTwoTone style={{ fontSize: 20 }} twoToneColor='#eb2f96' />
            </Col>
          </Row>
          <Row>
            <Col span={4} offset={8}>
              Итоговая оценка:
            </Col>
            <Col span={4}>
              <Input onChange={(event) => onChangeResult(event.target.value)} value={`${result}`} />
            </Col>
          </Row>
          <Space align='end'>
            <Button type='primary' htmlType='submit' onClick={onFinish}>
              Завершить оценку
            </Button>
            <Button htmlType='button' onClick={onCancel}>
              Отложить
            </Button>
          </Space>
        </Card>
      </Space>
    </div>
  );
}
