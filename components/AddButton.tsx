import { PlusOutlined } from "@ant-design/icons";
import { Button, Tooltip } from "antd";

import classes from "../styles/addbutton.module.css";

interface AddButtonProps {
  onClick: () => void;
  toolTip: string;
}

export default function AddButton({ onClick, toolTip }) {
  return (
    <div className={classes.AddButton}>
      <Tooltip title={toolTip}>
        <Button shape='circle' icon={<PlusOutlined />} size='large' type='primary' onClick={onClick} />
      </Tooltip>
    </div>
  );
}
