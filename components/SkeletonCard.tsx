import { Card, Skeleton, Space } from "antd";
import Title from "antd/lib/typography/Title";
import classes from "../styles/index.module.css";

export default function SkeletonCard({ title }) {
  return (
    <div className={classes.Main}>
      <Space direction='vertical' size='large' align='center' className={classes.Space}>
        <Card className={classes.DCcard} hoverable>
          <Title level={2}>{title}</Title>
          <Skeleton />
        </Card>
      </Space>
    </div>
  );
}
